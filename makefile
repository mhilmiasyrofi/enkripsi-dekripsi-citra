CC = g++
CFLAGS = `pkg-config --cflags --libs opencv`
OBJ = main.o \
		rsa.o

Grilya : $(OBJ)
		$(CC) $(OBJ) -o RSA $(CFLAGS)

				-rm $(OBJ)

%.o : %.c %.h

.PHONY : clean
clean :
				-rm RSA
