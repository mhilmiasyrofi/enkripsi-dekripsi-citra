/************************************
 * Nama  : Muhammad Hilmi Asyrofi
 * NIM   : 13515083
 * email : mhilmiasyrofi@gmail.com ; 13515083@std.stei.itb.ac.id
 * Program ini akan membaca input gambar dari command line kemudian melakukan enkripsi-dekripsi terhadap citra uji
 * *********************************/

#include <opencv2/opencv.hpp>
#include <iostream>
#include <time.h>
#include "rsa.h"


using namespace std;
using namespace cv;

static void help()
{
    cout
    << "\nProgram ini akan membaca input gambar dari command line kemudian melakukan enkripsi-dekripsi terhadap citra uji\n"
    << "Cara Run Program:\n"
    << "./RSA 'nama_gambar.jpg'\n"
    << endl;
}


int main( int argc, char** argv ){

    cv::CommandLineParser parser(argc, argv, "{help h||}");

    if (parser.has("help"))
    {
        help();
        return 0;
    }

    Mat image = imread(argv[1]);  //input gambar

    //Jika gambar tidak ditemukan
    if(!image.data){
      cout << "Gambar yang akan dienkripsi tidak ditemukan"<< endl;
      return 0;
    }

    //menampilkan Citra Asli
    namedWindow("Gambar Asli");
    imshow("Gambar Asli",image);

    //Inisialisasi RSA
    Rsa rsa;
    Key key = rsa.produce_keys();

    //Inisialisasi citra enkripsi
    Mat imencrypt;
    imencrypt.create(image.rows, image.cols, CV_32SC3);

    //Inisialisasi citra dekripsi
    Mat decrypt = image.clone();

    //Digunakan untuk mode CBC
    long prevB = 0;
    long prevG = 0;
    long prevR = 0;

    //Memulai menghitung Run Time
    clock_t tStart = clock();


    //ENKRIPSI
    for(int i =0; i < image.cols; i++){
      for(int k = 0; k < image.rows; k++){

        long r,g,b;
        r = (long)image.at<Vec3b>(k,i)[0];
        g = (long)image.at<Vec3b>(k,i)[1];
        b = (long)image.at<Vec3b>(k,i)[2];

        r = r^prevR;
        g = g^prevG;
        b = b^prevB;

        r = rsa.endecrypt(r, key.ekey, key.pkey);
        g = rsa.endecrypt(g, key.ekey, key.pkey);
        b = rsa.endecrypt(b, key.ekey, key.pkey);

        prevR = r;
        prevG = g;
        prevB = b;

        imencrypt.at<Vec3i>(k,i)[0] = r;
        imencrypt.at<Vec3i>(k,i)[1] = g;
        imencrypt.at<Vec3i>(k,i)[2] = b;

      }
    }

    //menampilkan citra enkripsi
    namedWindow("Gambar enkripsi");
    imshow("Gambar enkripsi",imencrypt);

    prevB = 0;
    prevG = 0;
    prevR = 0;


    //DEKRIPSI
    for(int i =0; i < image.cols; i++){
      for(int k = 0; k < image.rows; k++){

        long r,g,b;

        r = imencrypt.at<Vec3i>(k,i)[0];
        g = imencrypt.at<Vec3i>(k,i)[1];
        b = imencrypt.at<Vec3i>(k,i)[2];

        r = rsa.endecrypt(r, key.dkey, key.pkey);
        g = rsa.endecrypt(g, key.dkey, key.pkey);
        b = rsa.endecrypt(b, key.dkey, key.pkey);

        r = r^prevR;
        g = g^prevG;
        b = b^prevB;

        prevR = (long)imencrypt.at<Vec3i>(k,i)[0];
        prevG = (long)imencrypt.at<Vec3i>(k,i)[1];
        prevB = (long)imencrypt.at<Vec3i>(k,i)[2];

        decrypt.at<Vec3b>(k,i)[0] = r;
        decrypt.at<Vec3b>(k,i)[1] = g;
        decrypt.at<Vec3b>(k,i)[2] = b;

      }
    }

    //menampilkan Citra Dekripsi
    namedWindow("Gambar dekripsi");
    imshow("Gambar dekripsi",decrypt);

    //menampilkan waktu running time
    printf("Lama waktu running time: %.10fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

    waitKey(50000);


    return 1;
}
