# Tugas Makalah IF2120 Matematika Diskrit Semester I Tahun 2016/2017


## Penerapan Algoritma RSA dan Mode CBC (*Chiper Block Chaining*) untuk Enkripsi-Dekripsi Citra Digital

<br />

## Author
* **Muhammad Hilmi Asyrofi** - **13515083** - [Facebook](https://www.facebook.com/muhammad.h.asyrofi)

<br />

## Cara Kompilasi

```
make
```
<br />

## Cara Run Program

```
./RSA <nama_gambar.jpg>
```
<br />

## Makalah
klik [*disini*](https://drive.google.com/open?id=0ByhkBZAQqBWMbGxRa2FOdXhqRjA) untuk mengunduh

<br />
<br />

![](interface.png)
